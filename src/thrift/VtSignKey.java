/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thrift;

import contract.user.UserRegisterResponse;
import contract.user.UserService;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;

/**
 *
 * @author alfaridi
 */
public class VtSignKey {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println(testRegister());
    }
    
    private static String testRegister() {
        TTransport transport = new TSocket("128.199.124.123", 7666);

        UserRegisterResponse reg = new UserRegisterResponse();
        try {
            transport.open();

            TProtocol protocol = new TBinaryProtocol(transport);
            UserService.Client client = new UserService.Client(protocol);

            reg = client.userRegister("abcs@gmail.com", "aabbccddee123", UUID.randomUUID().toString());
    
            transport.close();
        } catch (TException t) {
            t.printStackTrace();
            transport.close();
        }

        return reg.getEmail();
    }
    
    private static String testThrift() {
        TTransport transport = new TSocket("128.199.124.123", 7666);

        String ping = "";
        try {
            transport.open();

            TProtocol protocol = new TBinaryProtocol(transport);
            UserService.Client client = new UserService.Client(protocol);

            ping = client.ping();

            transport.close();
        } catch (TException t) {
            t.printStackTrace();
            transport.close();
        }

        return ping;
    }
    
    private static void testSepulsa() {
        String orderId = "sepulsa-14497324751";
        String statusCode = "200";
        String grossAmount = "25000.00";
        String serverKey = "VT-server-ee92NebFBtA7IZ-lSkrDQXux";
        
        StringBuilder toHash = new StringBuilder()
                .append(orderId)
                .append(statusCode)
                .append(grossAmount)
                .append(serverKey);
        
        System.out.println(signatureKey(toHash.toString()));
    }
    
    public static String signatureKey(String strToHash) {
        String result = "";

        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-512");
            md.update(strToHash.getBytes());

            byte byteData[] = md.digest();

            //convert the byte to hex format method 1
            StringBuilder sb = new StringBuilder();
            for (byte aByteData : byteData) {
                sb.append(Integer.toString((aByteData & 0xff) + 0x100, 16).substring(1));
            }

            result = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return result;
    }
    
}
