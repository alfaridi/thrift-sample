/**
 * Autogenerated by Thrift Compiler (0.9.2)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package contract.payment;


import java.util.Map;
import java.util.HashMap;
import org.apache.thrift.TEnum;

public enum PaymentType implements org.apache.thrift.TEnum {
  CREDIT_CARD(0),
  BANK_TRANSFER(1),
  PERMATA(2),
  BCA(3),
  MANDIRI_BILL(4),
  BBM_MONEY(5);

  private final int value;

  private PaymentType(int value) {
    this.value = value;
  }

  /**
   * Get the integer value of this enum value, as defined in the Thrift IDL.
   */
  public int getValue() {
    return value;
  }

  /**
   * Find a the enum type by its integer value, as defined in the Thrift IDL.
   * @return null if the value is not found.
   */
  public static PaymentType findByValue(int value) { 
    switch (value) {
      case 0:
        return CREDIT_CARD;
      case 1:
        return BANK_TRANSFER;
      case 2:
        return PERMATA;
      case 3:
        return BCA;
      case 4:
        return MANDIRI_BILL;
      case 5:
        return BBM_MONEY;
      default:
        return null;
    }
  }
}
